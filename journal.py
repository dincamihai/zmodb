import logging
import transaction
from zc.lockfile import LockError
from pymongo import Connection
from ZODB.FileStorage import FileStorage
from ZODB.DB import DB


def behave(action):
    def wrapper(self, *args, **kwargs):
        #initialize connection
        zodb = {}
        zodb['storage'] = FileStorage("zodb.fs")
        zodb['db'] = DB(zodb['storage'])
        zodb['connection'] = zodb['db'].open()
        zodb['root'] = zodb['connection'].root()
        self.zodb = zodb

        try:
            #perform action
            output = None
            if '_id' in kwargs:
                action(self, _id=kwargs['_id'])
            elif 'key' in kwargs:
                output = action(self, key=kwargs['key'])
            elif 'row' in kwargs:
                output = action(self, data=kwargs['row'])
            else:
                action(self)
            return output
        except LockError as error:
            logging.warning('%s' %error)
        except KeyError as error:
            raise error
        finally:
            #close connection
            transaction.get().abort()
            self.zodb['connection'].close()
            self.zodb['db'].close()
            self.zodb['storage'].close()
    return wrapper


class Journal:


    def __init__(self, row):
        self.row = row


    @behave
    def get(self, key=None):
        try:
            output =  self.zodb['root'][str(key)]
            return output
        except KeyError as error:
            raise error


    @behave
    def register(self):
        collection = self.row.collection
        value = collection.find_one({'_id': self.row.data['_id']})
        self.zodb['root'][str(self.row.data['_id'])] = value
        transaction.get().commit()
        logging.info('Journal annotated.')


    @behave
    def close(self):
        logging.info('Journal closed.')


    @behave
    def drop(self):
        self.zodb['root'].clear()
        transaction.get().commit()
        logging.info('Journal dropped.')


    @behave
    def update(self, data=None):
        collection = self.row.collection
        row = collection.find_one({'_id': data['_id']})
        self.zodb['root'][str(row['_id'])] = row
        transaction.get().commit()
        logging.info('Journal updated.')


    @behave
    def remove(self, _id=None):
        del self.zodb['root'][str(_id)]
        transaction.get().commit()
        logging.info('Journal entry deleted.')


class Row:


    def __init__(self, data=None):
        self.data = data
        self.journal = Journal(self)


    def save(self):
        self.data['_id'] = self.collection.insert(self.data)
        self.journal.row = self
        self.journal.register()


    def update(self, data):
        self.collection.update({'_id': data['_id']}, data)
        self.data = data
        self.journal.update(row=self.data)


    def delete(self):
        self.collection.remove(self.data['_id'])
        self.journal.remove(_id=str(self.data['_id']))


    @property
    def collection(self):
        mongo_conn = Connection()
        mongo_db = mongo_conn.test #db name
        collection = mongo_db.entries #collection name
        return collection
