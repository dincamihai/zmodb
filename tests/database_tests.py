import unittest
import logging


logging.basicConfig(level=logging.INFO)
COUNTRY_DATA = {
    "fr":"Cameroun",
    "en":"Cameroon",
    "sp":"Camer\u00fan",
    "iso": "CM",
}


def setUpModule():
    global zmodb
    global Connection
    global FileStorage
    global DB
    global journal

    import transaction
    from pymongo import Connection
    from ZODB.FileStorage import FileStorage
    from ZODB.DB import DB
    from zmodb import journal


class RowCrudTest(unittest.TestCase):


    def tearDown(self):
        with Connection() as mongo_conn:
            mongo_db = mongo_conn.test #db name
            collection = mongo_db.entries #collection name
            row = journal.Row()
            row.collection.drop()
            row.journal.drop()


    def test_create(self):
        row = journal.Row(COUNTRY_DATA)
        row.save()

        with Connection() as mongo_conn:
            mongo_db = mongo_conn.test #db name
            collection = mongo_db.entries #collection name
            mongo_row = collection.find_one(row.data['_id'])
            self.assertEqual(row.journal.get(key=row.data['_id']),
                             collection.find_one(row.data['_id']))


    def test_update(self):
        row = journal.Row(COUNTRY_DATA)
        row.save()

        with Connection() as mongo_conn:
            mongo_db = mongo_conn.test #db name
            collection = mongo_db.entries #collection name
            mongo_row = collection.find_one(row.data['_id'])
            _id = mongo_row['_id']
            mongo_row['iso'] = 'CA'   #change
            mongo_row['region'] = '1' #add
            del mongo_row['sp']       #delete
            row.update(data = mongo_row)
            self.assertEqual(row.journal.get(key=_id),
                             collection.find_one(_id))


    def test_delete(self):
        row = journal.Row(COUNTRY_DATA)
        row.save()

        with Connection() as mongo_conn:
            mongo_db = mongo_conn.test #db name
            collection = mongo_db.entries #collection name
            temp_row = collection.find_one(row.data['_id'])
            temp = journal.Row(temp_row)
            temp.delete()

        self.assertEqual(collection.find_one(row.data['_id']), None)
        import re
        regexp = re.compile('%s' % str(row.data['_id']))

        def fun():
            try:
                temp.journal.get(key=row.data['_id']) #should raise KeyError
            except KeyError:
                raise

        self.assertRaisesRegexp(KeyError, str(row.data['_id']), fun)
